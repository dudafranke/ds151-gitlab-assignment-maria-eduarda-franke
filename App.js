import React from 'react';
import { Alert } from 'react-native';

import { navigationRef } from './RootNavigation';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator,  DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { AuthProvider } from "./src/context/AuthContext";
import { UserIdProvider } from "./src/context/UserIdContext";
import { UsernameProvider } from "./src/context/UsernameContext";

import LoginScreen from './src/screens/LoginScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import ProjectsScreen from './src/screens/ProjectsScreen';

const Drawer = createDrawerNavigator();
const Tab = createMaterialTopTabNavigator();
const Stack = createNativeStackNavigator();

const CustomDrawerContent= ({ props, navigation }) => {

  function handlePress() {
    AsyncStorage.clear()
    navigation.navigate("Login")
  }

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label="Logout" onPress={() => 
                                  Alert.alert(
                                    "Are your sure?",
                                    "Are you sure you want to logout?",
                                    [
                                      // The "Yes" button
                                      {
                                        text: "Yes",
                                        onPress: () => {
                                          handlePress()
                                        },
                                      },
                                      // The "No" button
                                      // Does nothing but dismiss the dialog when tapped
                                      {
                                        text: "No",
                                      },
                                    ]
                                  )
                                  } />
    </DrawerContentScrollView>
  );
}

function AppDrawerScreen({navigation}) {
  return (
    <Drawer.Navigator initialRouteName="Projects" drawerContent={props => <CustomDrawerContent 
                                                                            props = {{...props}}
                                                                            navigation = {navigation}
                                                                          />}>
        <Drawer.Screen name="GitLab Projects" component={ProjectsScreen} />
        <Drawer.Screen name="Profile" component={ProfileScreen} />
    </Drawer.Navigator>
  );
}

function App() {
  return (
    <AuthProvider>
      <UserIdProvider>
        <UsernameProvider>
          <NavigationContainer ref={navigationRef}>
            <Stack.Navigator screenOptions={{
              headerShown: false
            }}>
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="AppDrawer" component={AppDrawerScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </UsernameProvider>
      </UserIdProvider>
    </AuthProvider>
  );
}

export default App;